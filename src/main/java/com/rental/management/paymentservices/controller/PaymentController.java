package com.rental.management.paymentservices.controller;


import com.rental.management.paymentservices.model.Payment;
import com.rental.management.paymentservices.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/payments")
public class PaymentController
{
    @Autowired
    private PaymentService paymentService;

    @GetMapping
    public List<Payment> findAllByLandlordId(@RequestParam("landlordId") Long id){
        return paymentService.findByLandlordId(id);
    }

    @PostMapping("/save")
    public Payment save(@RequestBody Payment payment)
    {
        return paymentService.save(payment);
    }


    @PutMapping("/update")
    public Payment update(@RequestBody Payment payment)
    {
        return paymentService.update(payment);
    }

    @GetMapping("/one/{id}")
    public Payment getPaymentById(@PathVariable("id") Long id)
    {
        return paymentService.getById(id);
    }


    @GetMapping("/all")
    public List<Payment> getAll()
    {
        return paymentService.getAll();
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id)
    {
        paymentService.delete(id);
    }
}
