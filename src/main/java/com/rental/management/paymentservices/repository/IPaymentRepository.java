package com.rental.management.paymentservices.repository;

import com.rental.management.paymentservices.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IPaymentRepository extends JpaRepository<Payment,Long>
{
    List<Payment> findPaymentsByLandlordId(Long id);



}
