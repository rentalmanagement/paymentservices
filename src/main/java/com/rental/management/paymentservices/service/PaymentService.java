package com.rental.management.paymentservices.service;

import com.rental.management.paymentservices.model.Payment;
import com.rental.management.paymentservices.repository.IPaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
public class PaymentService
{
    @Autowired
    private IPaymentRepository paymentRepository;

    @Transactional(readOnly = true)
    public Payment getById(Long id)
    {
        return paymentRepository.getOne(id);
    }

    @Transactional
    public List<Payment> getAll()
    {
        return paymentRepository.findAll();
    }

    @Transactional
    public Payment save(Payment payment)
    {
        return paymentRepository.save(payment);
    }

    @Transactional
    public Payment update(Payment payment)
    {
        Optional<Payment> optPat=
        paymentRepository.findById(payment.getId());
        if(optPat.isPresent())
        {
           return paymentRepository.save(payment);
        }
        return payment;
    }

    @Transactional
    public void delete(Long id)
    {
        paymentRepository.deleteById(id);
    }

    @Transactional
    public List<Payment> findByLandlordId(Long id)
    {
      return  paymentRepository.findPaymentsByLandlordId(id);
    }
}
