package com.rental.management.paymentservices.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "payments")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Payment
{
   @Id
   @GeneratedValue(strategy =  GenerationType.IDENTITY)
   @Column(name = "id",unique = true)
    private Long id;
    @Column(name = "userId",nullable = false)
    private Long userId;


    @Column(name = "landlordId",nullable = false)
    private Long landlordId;

    @Column(name = "amount",nullable = false)
    private double amount;

    public Payment()
    {
    }

    public Payment(Long userId, double amount) {
        this.userId = userId;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Long getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(Long landlordId) {
        this.landlordId = landlordId;
    }
}
